const grid = require("./grid");
const robot = require("./robot");

describe("createGrid", () => {
  it("returns false if there's a missing input value", () => {
    expect.assertions(1);
    const result = grid.createGrid(14);

    expect(result).toBeFalsy();
  });
  it("returns false if any of the input values is higher than 50", () => {
    expect.assertions(1);
    const result = grid.createGrid(12, 53);

    expect(result).toBeFalsy();
  });
  it("returns true if two values under 50 are sent through", () => {
    expect.assertions(1);
    const result = grid.createGrid(13, 15);

    expect(result).toBeTruthy();
  });
});

describe("validatePosition", () => {
  it("returns false if the position of the robot is out of bounds", () => {
    expect.assertions(1);
    grid.createGrid(15, 15);
    const result = grid.validatePosition(16, 11);

    expect(result).toBeFalsy();
  });
  it("returns true if the position of the robot is within bounds", () => {
    expect.assertions(1);
    grid.createGrid(15, 15);
    const result = grid.validatePosition(9, 11);

    expect(result).toBeTruthy();
  });
});

describe("processInstructions", () => {
  // SAMPLE INPUT FOR SUCCESS
  it("returns true if all the instructions are processed successfully", () => {
    expect.assertions(1);
    grid.createGrid(5, 3);
    const result = grid.processInstructions("RFRFRFRF", 1, 1, "E");

    expect(result).toBeTruthy();
  });
  // SAMPLE INPUT FOR LOST ROBOT
  it("returns false if the robot was lost", () => {
    expect.assertions(1);
    grid.createGrid(5, 3);
    const result = grid.processInstructions("FRFLLFFRRFLL", 3, 2, "N");

    expect(result).toBeFalsy();
  });
});
