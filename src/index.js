const inquirer = require("inquirer");

const grid = require("./grid");

console.log("Welcome, explorer! You are in Mars.");
const questions = [
  {
    type: "input",
    name: "width",
    message: "Enter grid width: "
  },
  {
    type: "input",
    name: "length",
    message: "Enter grid length: "
  }
];

const position = [
  {
    type: "input",
    name: "x",
    message: "Enter x coordinate: "
  },
  {
    type: "input",
    name: "y",
    message: "Enter y cordinate: "
  },
  {
    type: "input",
    name: "o",
    message: "Enter orientation: "
  }
];

const instructions = [
  {
    type: "input",
    name: "string",
    message: "Enter instructions: "
  }
];

inquirer.prompt(questions).then(answers => {
  const validGrid = grid.createGrid(answers["width"], answers["length"]);
  if (validGrid) {
    console.log("deploying robot");
    inquirer.prompt(position).then(coordinates => {
      if (grid.validatePosition(coordinates["x"], coordinates["y"])) {
        inquirer.prompt(instructions).then(instr => {
          const str = instr["string"];
          if (str.length < 100) {
            const result = grid.processInstructions(
              str,
              coordinates["x"],
              coordinates["y"],
              coordinates["o"]
            );
            console.log("did your robot make it? :" + result);
          } else {
            console.log("Robots can't take more than 100 instructions!");
          }
        });
      } else {
        console.log("Don't kill your robot!");
      }
    });
  } else {
    console.log("Grid must be defined by two digits under 50");
  }
});
