const robot = require("./robot");

describe("robot actions", () => {
  it("returns x, y+1 if robot is facing N", () => {
    expect.assertions(3);

    const expected = {
      x: 4,
      y: 6,
      o: "N"
    };

    const result = robot.moveRobot(4, 5, "N");

    expect(result.x1).toEqual(expected.x);
    expect(result.y1).toEqual(expected.y);
    expect(result.o1).toEqual(expected.o);
  });
  it("returns x, y-1 if robot is facing south", () => {
    expect.assertions(3);

    const expected = {
      x: 4,
      y: 4,
      o: "S"
    };

    const result = robot.moveRobot(4, 5, "S");

    expect(result.x1).toEqual(expected.x);
    expect(result.y1).toEqual(expected.y);
    expect(result.o1).toEqual(expected.o);
  });
  it("returns x+1,y if robot is facing east", () => {
    expect.assertions(3);

    const expected = {
      x: 5,
      y: 5,
      o: "E"
    };

    const result = robot.moveRobot(4, 5, "E");

    expect(result.x1).toEqual(expected.x);
    expect(result.y1).toEqual(expected.y);
    expect(result.o1).toEqual(expected.o);
  });
  it("returns x-1,y if robot is facing west", () => {
    expect.assertions(3);

    const expected = {
      x: 3,
      y: 5,
      o: "W"
    };

    const result = robot.moveRobot(4, 5, "W");

    expect(result.x1).toEqual(expected.x);
    expect(result.y1).toEqual(expected.y);
    expect(result.o1).toEqual(expected.o);
  });
  it("returns N if robot is facing W and receives R", () => {
    expect.assertions(1);

    const result = robot.rotateRobot("W", "R");
    expect(result).toEqual("N");
  });
  it("returns N if robot is facing E and receives L", () => {
    expect.assertions(1);

    const result = robot.rotateRobot("E", "L");
    expect(result).toEqual("N");
  });
  it("returns S if robot is facing W and receives L", () => {
    expect.assertions(1);

    const result = robot.rotateRobot("W", "L");
    expect(result).toEqual("S");
  });
  it("returns S if robot is facing E and receives R", () => {
    expect.assertions(1);

    const result = robot.rotateRobot("E", "R");
    expect(result).toEqual("S");
  });
  it("returns E if robot is facing N and receives R", () => {
    expect.assertions(1);

    const result = robot.rotateRobot("N", "R");
    expect(result).toEqual("E");
  });
  it("returns E if robot is facing S and receives L", () => {
    expect.assertions(1);

    const result = robot.rotateRobot("S", "L");
    expect(result).toEqual("E");
  });
  it("returns W if robot is facing S and receives R", () => {
    expect.assertions(1);

    const result = robot.rotateRobot("S", "R");
    expect(result).toEqual("W");
  });
  it("returns W if robot is facing N and receives L", () => {
    expect.assertions(1);

    const result = robot.rotateRobot("N", "L");
    expect(result).toEqual("W");
  });
});
