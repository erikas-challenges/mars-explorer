const robot = require("./robot");

let w;
let l;

function createGrid(width, length) {
  if (width && length && (width < 50 && length < 50)) {
    w = width;
    l = length;
    return true;
  }
  return false;
}

function validatePosition(x, y) {
  if (+x < w && +y < l) {
    return true;
  }
  return false;
}

function processInstructions(instructions, x, y, dir) {
  let newX = x;
  let newY = y;
  let newO = dir;
  let newPos;

  for (let i = 0; i < instructions.length; i += 1) {
    if (instructions[i] == "R" || instructions[i] == "L") {
      newO = robot.rotateRobot(newO, instructions[i]);
    }
    if (instructions[i] == "F") {
      newPos = robot.moveRobot(newX, newY, newO);
      if (newPos.x1 < w && newPos.y1 < l) {
        newX = newPos.x1;
        newY = newPos.y1;
        newO = newPos.o1;
      } else {
        console.log("Robot has been lost");
        console.log(newX + " " + newY + " " + newO);
        return false;
      }
    }
  }
  console.log("Robot completed the instructions");
  console.log(newX + " " + newY + " " + newO);
  return true;
}

module.exports = { createGrid, validatePosition, processInstructions };
