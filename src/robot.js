const availableDirection = ["N", "E", "S", "W"];

const moveRobot = (x, y, o) => {
  let x1 = x;
  let y1 = y;
  let o1 = o;
  if (o == "N") {
    y1 += 1;
  }
  if (o == "S") {
    y1 -= 1;
  }
  if (o == "E") {
    x1 += 1;
  }
  if (o == "W") {
    x1 -= 1;
  }
  return { x1, y1, o1 };
};

const rotateRobot = (o, dir) => {
  const indexLocation = availableDirection.indexOf(o);
  if (dir == "L") {
    if (indexLocation === 0) {
      dir = availableDirection[3];
    } else {
      dir = availableDirection[indexLocation - 1];
    }
  }
  if (dir == "R") {
    if (indexLocation === 3) {
      dir = availableDirection[0];
    } else {
      dir = availableDirection[indexLocation + 1];
    }
  }
  return dir;
};

module.exports = { moveRobot, rotateRobot };
