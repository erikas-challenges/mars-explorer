Coding Challenge 30/10/2019

Instrucciones:

1. npm install
2. npm start

TODO:

1. Dar la opción de crear un robot nuevo cuando se pierde el anterior
2. Dejar el rastro en el grid cuando un robot se pierde
3. Detener la lectura de instrucciones cuando se pierde el robot
4. Permitir ingresar más instrucciones cuando el robot sobrevive
